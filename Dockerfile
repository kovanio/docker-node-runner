FROM kovan/node

RUN apk add --update git build-base python

ENV GREF master

WORKDIR /
ADD run.sh .

EXPOSE 3000

CMD ["/run.sh"]
