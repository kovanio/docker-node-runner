#!/bin/sh

git clone "https://$USER:$PASS@$REPO" app && cd app
git checkout $GREF

npm install
npm start
